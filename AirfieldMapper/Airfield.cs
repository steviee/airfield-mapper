﻿using System;
using System.Collections.Generic;
using System.Linq;
using NLog;
using QuikGraph;
using QuikGraph.Algorithms;
using YamlDotNet.Serialization;

namespace RurouniJones.DCS.AirfieldMapper
{
    public class Airfield
    { 
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        
        /// <summary>
        /// Name of the Airfield.
        /// </summary>
        [YamlMember(Alias = "name")]
        public string Name { get; set; }

        /// <summary>
        /// Decimal latitude (e.g. 41.12324)
        /// </summary>
        [YamlMember(Alias = "lat")]
        public double Latitude { get; set; }

        /// <summary>
        /// Decimal Longitude (e.g. 37.12324)
        /// </summary>
        [YamlMember(Alias = "lon")]
        public double Longitude { get; set; }

        /// <summary>
        /// Altitude in Meters
        /// </summary>
        [YamlMember(Alias = "alt")]
        public double Altitude { get; set; }

        /// <summary>
        /// A list of Parking spots
        /// 
        /// A Parking Spot includes anywhere where aircraft might spawn. So this needs to cover areas where a mission
        /// maker might spawn "OnGround" aircraft such as Harriers. For this reason the Parking Spots at Anapa include
        /// the Maintenance Area as Harriers are spawned there on GAW.
        /// </summary>
        [YamlMember(Alias = "parking_spots")]
        public HashSet<ParkingSpot> ParkingSpots { get; set; } = new HashSet<ParkingSpot>();

        /// <summary>
        /// A list of Runways 
        /// </summary>
        [YamlMember(Alias = "runways")]
        public HashSet<Runway> Runways { get; set; } = new HashSet<Runway>();

        /// <summary>
        /// A list of paths that link nodes that an aircraft can travel along.
        /// 
        /// A  with a specific source and target NavigationPoint.
        /// If it is possible to go in both directions between nodes then there need to be two navigation paths, one going each way.
        /// </summary>
        [YamlMember(Alias = "navigation_paths")]
        public List<NavigationPath> NavigationPaths { get; set; } = new List<NavigationPath>();

        /// <summary>
        /// A list of Taxi Junctions
        /// 
        /// A Taxi Junction is any place where two taxiways meet each other and where they meet either a Parking Spot
        /// or a Runway
        /// </summary>
        [YamlMember(Alias = "junctions")]
        public HashSet<Junction> Junctions { get; set; } = new HashSet<Junction>();

        /// <summary>
        ///     See QuikGraph for more information on Tagged edges
        /// </summary>
        [YamlIgnore]
        public Dictionary<TaggedEdge<NavigationPoint, string>, double> NavigationCost;

        /// <summary>
        ///     See QuikGraph for more information on navigation costs
        /// </summary>
        [YamlIgnore]
        public Func<TaggedEdge<NavigationPoint, string>, double> NavigationCostFunction;

        /// <summary>
        ///     See QuikGraph for more information on graph types
        /// </summary>
        [YamlIgnore]
        public AdjacencyGraph<NavigationPoint, TaggedEdge<NavigationPoint, string>> NavigationGraph =
            new AdjacencyGraph<NavigationPoint, TaggedEdge<NavigationPoint, string>>();

        [YamlIgnore]
        public Dictionary<Runway, List<NavigationPoint>> RunwayNodes { get; set; } = new Dictionary<Runway, List<NavigationPoint>>();

        public void BuildTaxiGraph()
        {
            try
            {
                Logger.Debug($"{Name} airfield JSON deserialized");

                Runways.ToList().ForEach(runway => NavigationGraph.AddVertex(runway));
                ParkingSpots.ToList().ForEach(parkingSpot => NavigationGraph.AddVertex(parkingSpot));
                Junctions.ToList().ForEach(junction => NavigationGraph.AddVertex(junction));
                WayPoints.ToList().ForEach(wayPoint => NavigationGraph.AddVertex(wayPoint));

                NavigationCost = new Dictionary<TaggedEdge<NavigationPoint, string>, double>(NavigationGraph.EdgeCount);

                foreach (var navigationPath in NavigationPaths)
                {
                    var source = NavigationGraph.Vertices.First(taxiPoint => taxiPoint.Name.Equals(navigationPath.Source));
                    var target = NavigationGraph.Vertices.First(taxiPoint => taxiPoint.Name.Equals(navigationPath.Target));
                    var tag = navigationPath.Name;

                    var edge = new TaggedEdge<NavigationPoint, string>(source, target, tag);

                    NavigationGraph.AddEdge(edge);

                    NavigationCost.Add(edge, navigationPath.Cost);
                }

                NavigationCostFunction = AlgorithmExtensions.GetIndexer(NavigationCost);
                Logger.Debug($"{Name} airfield navigation graph built");
            }
            catch (Exception e)
            {
                Logger.Error(e, $"Could not build navigation graph for {Name}");
            }

            // Now populate the runway nodes so we know all the nodes that make up a runway
            try
            {
                foreach(var runway in Runways)
                {
                    NavigationPoint node = runway;
                    var nodes = new List<NavigationPoint> {node};
                    while (true)
                    {
                        var edges = NavigationGraph.Edges.Where(x => x.Source == node);
                        try
                        {
                            node = edges.First(x => x.Tag == "Runway" && !nodes.Contains(x.Target)).Target;
                            nodes.Add(node);
                            if (node is Runway) break;

                        }
                        catch (InvalidOperationException)
                        {
                            break;
                        }
                    }
                    RunwayNodes.Add(runway, nodes);
                    Logger.Debug($"{Name} {runway} nodes built");
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, $"Could not build runway nodes for  graph for {Name}");
            }
        }

        /// <summary>
        /// A list of Waypoints for inbound and outbound aircraft that are in the air.
        /// </summary>
        [YamlMember(Alias = "waypoints")]
        public HashSet<WayPoint> WayPoints { get; set; } = new HashSet<WayPoint>();

        public class NavigationPoint
        {
            [YamlMember(Alias = "name")]
            public string Name { get; set; }

            [YamlMember(Alias = "lat")]
            public double Latitude { get; set; }

            [YamlMember(Alias = "lon")]
            public double Longitude { get; set; }

            public override string ToString()
            {
                return $"{Name}: Lat {Latitude} / Lon {Longitude}";
            }
        }

        /// <summary>
        /// An airborne waypoint used for approach and departure handling
        /// </summary>
        public class WayPoint : NavigationPoint {}

        public class TaxiPoint : NavigationPoint {}

        public class ParkingSpot : TaxiPoint {}

        public class Junction : TaxiPoint {}

        public class Runway : TaxiPoint
        {
            [YamlMember(Alias = "heading")]
            public int Heading { get; set; }
        }

        public class NavigationPath
        {
            [YamlMember(Alias = "name")]
            public string Name { get; set; }

            [YamlMember(Alias = "source")]
            public string Source { get; set; }

            [YamlMember(Alias = "target")]
            public string Target { get; set; }

            [YamlMember(Alias = "cost")]
            public int Cost { get; set; }
        }
    }
}
